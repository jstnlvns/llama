#!/usr/bin/env python

import sys, re, time, os, shutil
import jinja2, markdown2
import datetime
import frontmatter
import pygments

BASEURL = ""
DRAFTSOURCE = "drafts/"
POSTSOURCE = "posts/"
DESTINATION = "public/"
LAYOUTSOURCE = "layouts/"
SHOWONHOME = 5
LAYOUTS = {
	"base": "base.html",
	"post": "post.html",
	"home": "home.html",
	"tags": "tags.html",
	"taggedposts": "taggedposts.html"
}

ENTRY_TIME_FORMAT = "%Y-%m-%d"
FORMAT = lambda text: markdown2.markdown(text, extras=['fenced-code-blocks'])

def getposts(postsdirectory):
	posts = []
	tags = []
	for root, dirs, files in os.walk(postsdirectory,topdown=True):
		for file in files:
			postlocation = "{}{}".format(root,file)
			post = frontmatter.load(postlocation)
			metadata = post.metadata
			posttags = metadata['tags'].split(',')
			metadata['url'] = file
			metadata['posturl'] = str(file)[:-3] + '.html'
			content = post.content
			formattedcontent = markdown2.markdown(content, extras=['fenced-code-blocks'])
			post.content = formattedcontent
			posts.append(post)
			for tag in posttags:
				tags.append(tag.strip())
	posts_sorted = sorted(posts, key=lambda p: p.metadata['date'])
	uniquetags = set(tags)
	tags = list(uniquetags)
	tags.sort()
	return posts_sorted, tags

def createhomepage(posts,env):
	layout = env.get_template(LAYOUTS['home'])
	url = 'index.html'
	writefile(url, layout.render(entries=posts))

def genposts(posts,env):
	layout = env.get_template(LAYOUTS['post'])
	for post in posts:
		filename = post.metadata['url'][:-3] + '.html'
		writefile(filename, layout.render(entry=post))

def gentagpages(posts,env,tags):
	layout = env.get_template(LAYOUTS['taggedposts'])
	tagbasedir = "{}/{}".format(DESTINATION,'tags')
	shutil.rmtree(tagbasedir)
	os.mkdir(tagbasedir)
	taglayout = env.get_template(LAYOUTS['tags'])
	tagindex = 'tags/index.html'
	writefile(tagindex, taglayout.render(tags=tags))
	for tag in tags:
		tagposts = []
		tagdir = "{}/{}".format(tagbasedir,tag)
		os.mkdir(tagdir)
		tagfile = "tags/{}/{}".format(tag,'index.html')
		for post in posts:
			posttags = post.metadata['tags'].split(',')
			if tag in posttags:
				tagposts.append(post)
		writefile(tagfile, layout.render(entries=tagposts))

def writefile(posturl, data):
	path =  DESTINATION + posturl
	file = open(path, 'w')
	file.write(data.encode('UTF-8'))
	file.close()

posts, tags = getposts(POSTSOURCE)
env = jinja2.Environment(loader=jinja2.FileSystemLoader(LAYOUTSOURCE))

genposts(posts, env)
createhomepage(posts, env)
gentagpages(posts,env,tags)

def fordebuggingposts(posts):
	for post in posts:
		print str(post)

fordebuggingposts(posts)
