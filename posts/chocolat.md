---
title: Chocolat
type: blog
date: 2017-11-08
category: review
tags: software,text editor,tools
---

My quest for a text editor I actually enjoy using may have come to halt. I came across [Chocolat][1] a while ago, but never gave it a real world test. I've been using it the past week or so and I have to say I really enjoy many of the features it offers.

Firstly, I don't do a lot big projects, normally most of the things I code are one-off scripts or data processing tasks. The need for a big, heavy-lifting IDE aren't really needed in my every day work.

The user interface is very clean -- no tab bar, no menu bar with buttons, the status bar at the bottom is out of the way.

![Chocolat IMG 1](resources/OVwCQ7h.png)

Building/Running your code can be using the Action menu or hitting command-R. What I particularly like, in this case it's a simple python script, Chocolat launches a terminal window to run the script rather than running it in an embedded output window. What I like about this is if your script/code has command line arguments or something you can do it more easily. Also, I enjoy seeing and working on the command line.

![Chocolat IMG 2](resources/f2958Wn.png)

It offers syntax highlighting for most all languages. You can vertical split the editing window simply by selecting multiple files in the file browser by holding shift and selecting the files you wish to split.


[1]: https://chocolatapp.com/
