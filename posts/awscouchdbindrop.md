---
title: Amazon AWS EC2 + CouchDB + Inkdrop
type: blog
date: 2022-05-16
category: review
tags: notetaking,amazonaws
---

I recently discovered [Inkdrop](https://inkdrop.app) as a potential replacement for [Quiver](https://happenapps.com) for my technical notebook.
Inkdrop offers several of same features as Quiver such as writing in Markdown, table rendering, code highlighting, to name a few -- Inkdrop does however offer a mobile app. While Quiver does have an iOS app, it's merely a reader so you're not able to edit any notes. With a companion mobile app comes note syncing and Inkdrop employs the use of [CouchDB](https://docs.couchdb.org/en/stable/) to facilitate its syncing. I didn't have any experience with CouchDB until I went down this rabbit hole of setting up my own sync service for Inkdrop.

Rather than going to any of the number of hosting sites out there I turned to Amazon AWS. Again, I didn't have any experience here either, so we'll see how it goes in the long run cost-wise. I'm sure the performance will be fine and for what I'm doing, the resources required is not heavy.

Spinning up an Ubuntu 18 instance couldn't have been easier here. To install CouchDB, I just followed the instructions from the [CouchDB docs](https://docs.couchdb.org/en/stable/install/unix.html). In order to install CouchDB, you do have to add it to the package repository.

```bash
sudo echo "deb https://apache.bintray.com/couchdb-deb bionic main" | sudo tee -a /etc/apt/sources.list
```

Run the install

```
sudo apt-get update && sudo apt-get install couchdb
```

The CouchDb server should spin right up and you can access the web interface for managing the database by going to `http://<<IP OF AMAZON EC3>>:5984/_utils/`. This seems like a good place to mention that you'll need to open up port `5984` on the VM to access it. This is done by going to the Security Groups page in the EC2 Console and finding the Group associated with your Ubuntu instance. Then select the Inbound tab on the bottom section and adding a new TCP rule allowing connection to port `5984` from anywhere.

The [Inkdrop FAQ](https://docs.inkdrop.app/manual/synchronizing-in-the-cloud#how-to-set-up-your-own-sync-server) also offers some documentation for setting up your own sync server.

### Mobile App Setup

If you don't want to have to change your connection string in the Inkdrop apps everytime you reboot your EC2, you need to setup an Elastic IP. There appears to be a cost associated with this, but it's fairly minimal. Basically just go into the EC2 Console and select Elastic IPs from Network & Security on the left, then Allocate new address. Setting up the sync for use with the mobile app did present the most trouble. Because of the stack being used SSL needs to be enabled and since I'm running this MacOS and iOS, my certificate couldn't be self-signed (this may be an issue because the desktop app is Electron).

#### SSL and Let's Encrypt

I'd used [Let's Encrypt](https://letsencrypt.org) before without much trouble - it's a great service and would recommend it for obtaining free signed certificates. Through this process I did learn you're not able to use Let's Encrypt to sign an IP address and it also didn't work with the domain/Public DNS supplied by my EC2. This really isn't a problem it just mean I needed to spend a little money to register for a domain and really it doesn't really matter what it is because I likely won't be used for anything else. After getting my domain registered and waiting a bit for it propogate, I was able to get the certificates through Let's Encrypt.

### $$$

Currently I'm in this for $5 which is what the domain cost. There will also be a cost for the EC2 but this will likely be pretty minimal I hope -- so far my usage is only up to $0.11 after about a week, the cost forecaster in AWS has me estimated at $0.32. I haven't migrated all my notes just yet, but for the most part the EC2 will mostly site idle and even during syncing the CPU remains pretty low. Inkdrop does offer their own sync service at $4.99 USD if you don't want to go through all this.
