---
title: Dropshare
type: blog
date: 2017-12-05
category: productivity
tags: productivity,sharing,tools
---

Recently I dropped [CloudApp](https://www.getcloudapp.com/) for quick file sharing. In my case, I am just not able to justify the cost vs. how much I used the app. I was on their PRO tier because many times I was doing screen recordings to show users how something worked and 15 seconds (FREE tier limit) wasn't going to cut it.

I have instead decided to give [Dropshare](https://getdropsha.re/) a try using my own storage. There is a cost upfront for the app, but ongoing the costs should be limited.